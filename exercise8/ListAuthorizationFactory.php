<?php
namespace Todo;

use XTilDone\ListAuthorization;

class ListAuthorizationFactory
{
    public function __invoke($services)
    {
        return new ListAuthorization($services->get('Todo\V2\Rest\Lists\Mapper'));
    }
}
