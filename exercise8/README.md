Exercise 8 Assets
=================

This directory contains the following assets to use with Exercise 8:

- `AuthorizationListener`, a class that registers itself to listen to
  authorization events in order to authorize specific activities within the Todo
  API. 
  
  Copy this file to `module/Todo/src/Todo/`. Then edit it to fill in the areas
  necessary in order to make authorization work.

- `AuthorizationListenerFactory`, a factory class for injecting the listener
  with its dependencies.
  
  Copy this file to `module/Todo/src/Todo/`.

- `ListAuthorizationFactory`, which returns an instance of
  `XTilDone\ListAuthorization`; this class provides the necessary logic for
  authenticating actions, and depends on having a `TableGateway` instance for
  the "lists" table -- which the factory provides.

  Copy this file to `module/Todo/src/Todo/`.

When done with the above, edit `module/Todo/config/module.config.php` to
register the two factories with the service manager.

Finally, edit `module/Todo/src/Todo/Module.php` to register the listener with
the "authorization" event, but only if the currently selected version >= 2.
