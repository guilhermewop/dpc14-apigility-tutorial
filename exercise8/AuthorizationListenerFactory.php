<?php
namespace Todo;

class AuthorizationListenerFactory
{
    public function __invoke($services)
    {
        return new AuthorizationListener($services->get('Todo\ListAuthorization'));
    }
}
