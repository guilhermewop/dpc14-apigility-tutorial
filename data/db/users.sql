INSERT INTO 'user' ('user_id', 'username', 'password', 'name') VALUES
    ('b171429f-ebfa-46aa-b4f1-e51fa8c7c2b5', 'vitae', '$2y$10$We4PF5uxHEQM1idP7Df0K.nIPxwvstoKjsxgt0Al8hKWJxQ0iYcZa', 'Ina Decker'),
    ('c42b0938-fc6d-4dc2-8e49-cfbe3fc06c75', 'massa', '$2y$10$ZE2lkHG7ftBz1Paexw2WQ.h9/lPf2HmIpAaPwAp4mQ6E6TYFqUEo.', 'Quinn Keller'),
    ('8950961a-f9be-4466-92d9-0bf20e8c36ff', 'ligula', '$2y$10$qqEbBmXRpQ94VDpxgoxpEuyddv3VpqL1/ZP.Llklu7gM8oBKo0/Aq', 'Rudyard Blake'),
    ('5d5785b8-22fe-48ee-a56f-3143088b9460', 'In', '$2y$10$vRKvZVB85mjZWbBxnq1Ckuo/HsWLJkY/DELukq3XVFQ2n2vryaN66', 'Linda Figueroa'),
    ('a47f8290-bab2-4623-b64c-15604d538487', 'gravida', '$2y$10$oxvEko2.Mm1cVW907S/UHOSWXcqzUu0IrIUhwEQHZR.5hjvuUfaiq', 'Neve Perez'),
    ('6298c025-48af-4e79-aed3-c63034ddd0fd', 'neque', '$2y$10$6GprOAV9cGEz.JuZWcTWVOi74gO6DXVg2TBgQChQ1wLM8IEgoNmNe', 'Bruno Mcfadden'),
    ('78395922-ab8d-4e8e-a8c3-e38fca5d570c', 'mauris', '$2y$10$hNhjTKLW7gZ/s78XKrtMkeJnTiq0iPVRE8ec9Aqp3eAPwLoEdXrSO', 'Oleg Barker'),
    ('f4f0fcc3-7cba-40de-8012-a2aa9f07cb84', 'Suspendisse', '$2y$10$/sJfvaz3D9n6HVSdNdkDiO3otVSO5Pc98lAuE7i4579XCmZAWh2GK', 'Ivory Livingston'),
    ('08c05c10-4619-4a9a-a063-379445910638', 'adipiscing', '$2y$10$ebXezw7TXR1.EstO7ZhI2.TVFmxwB8FE86LQ1aXsJZ04Xnl/U1D0C', 'Lenore Bright'),
    ('6220bd6c-9c65-455e-9062-9d6d0f4662a3', 'SierraShorti8W', '$2y$10$kFXreaF5V3yM/HEY9DTh7.Alndfa0rYY3dqmBMRUinvWyH1IL194e', 'Sierra Short'),
    ('730e8595-4193-435f-9e8d-5fa242c7e6b7', 'dolor', '$2y$10$JwgzTot0JrIDsPNkHNKsKu2PAGiW4gP0977AVDdSn7Q/BEWLkMeNu', 'Abdul Pratt'),
    ('ed7977aa-3daa-4432-bf19-537a3e8054f1', 'sed', '$2y$10$CmwAMFhQjfQePLDQxVMDSuWMmShWU64.APDfV2haD9hs.0yM6Ewce', 'Halla Wood'),
    ('116b39ba-5358-47f4-b804-2439720976e9', 'lorem', '$2y$10$3IV/TuE/P4KfDpyFPhPSbOGRix.zuNYlUFzIwgz7bWih0tfSkOr7a', 'Indigo Potts'),
    ('e57a632c-2a6a-493d-aa33-fe9491070d0c', 'porta', '$2y$10$bfJ9HriMCXgodH3X4SW.QOw0FlRvx3dJB7zpRfJ.mYxE0UjzQqpA2', 'Ingrid Frazier'),
    ('596c4044-1181-4e52-904a-a5f81996a1fa', 'Donec', '$2y$10$5oSOlmbuzJXafI8NLRgTdORbpmng.LbczHA4dNtOg50YhEmUVHVkG', 'Ebony Wagner'),
    ('fd7cdc84-e032-48a1-b9c4-8be50f0982c4', 'ClementineSawyerEGu', '$2y$10$8mGI6c9nHcTtk0coxRnnjOnahujjcIU1j/GqIp4SsUk6m8WeZoc2C', 'Clementine Sawyer'),
    ('d680b175-b4e1-47f7-8e6c-2e0fc07f54ef', 'Sed', '$2y$10$BlYw9lAbUf5EeFmkxyXsXuMYpJzsdIw0idBzcMU7vNmIkbcljMiXu', 'Dillon Oneill'),
    ('072af6af-a149-4b88-a9ac-88f18d4e0c9c', 'HadassahCurry0TQ', '$2y$10$SD99DtKbpRo8dmUJ2PDIR.rOrf9UcltqULBWIwsxdtAIoJHn7uhJK', 'Hadassah Curry'),
    ('f73e28ca-6501-434b-81e9-59ab0e618236', 'at', '$2y$10$cld/U6bZCLyFCwPMjc2rMOfDposE53pHwuDIXCPplgIB38Silxv7y', 'Ava Webb'),
    ('ea4ece20-8279-4a53-aaf4-344818868ad7', 'Duis', '$2y$10$o.Rl/oawDPuIR0M2Dhi80eirGs0LuyA/8MfmNw7XH9Sb4EVJDr5LW', 'Oliver Pierce'),
    ('c1ffda7a-b3b4-40c0-a57c-af497063cb9a', 'semper', '$2y$10$diS3tLwa1xCVMSk.ph/9suK17zNvYDrq/7pOHPkJ08C.WNMew1wWO', 'Unity Wheeler'),
    ('ff6ad650-ff0a-4393-9c0d-fee9910eaa1a', 'commodo', '$2y$10$BQUZ7xU2ts4wIN9q7kurXu5uBQWOqX0.s73VaFyaBiq5YrkHM.pCe', 'Kim Kirk'),
    ('6a21ff71-a5b4-4be1-bd26-5947fb9b4ef7', 'laoreet', '$2y$10$KKQaqKgL7VyT9TDBx9ZjG.mMIwgyZ83xKu/1To1kYhQxEhmC2AzNK', 'Melanie Acosta'),
    ('9fef1d5b-5ebd-492a-8d1f-2147a2a2ee8f', 'et', '$2y$10$FTpsdM8P8wrrKTKq2iwkAOeBrJxPIM5/jA7va8g2Xnstiu.AsUjGm', 'Slade David'),
    ('91e35e0f-e094-4188-9c4d-d3972d9ff2c9', 'CatherineBakerNOj', '$2y$10$ov9T.L37t9opOmuZVVViguzJRfST9AOjXYGGxD8KWiuYT0ZjeCV8a', 'Catherine Baker'),
    ('a0c4a0cc-de07-46fa-9354-7cb0a53ac1bb', 'BrynneBarnett0hX', '$2y$10$FJvSKf2cB8yKRR486FwFee4f51uCzIUfxX2JhbmVIra0Ru5o82i4y', 'Brynne Barnett'),
    ('88f733dc-0acf-4f9f-88fa-58fe063354ba', 'lobortis', '$2y$10$m0ug1sXjHgRs6XecPB2MCekn0zkiVLgHDuPh2lpdHD1rRd1ap76ou', 'Calvin Brennan'),
    ('54724fee-7089-4e31-9836-8a0718e0ec93', 'quis', '$2y$10$DnSdvkWdUvCPvousqkOgF.jc8OtGqs5GaiTA41ImlWRxsqMafqd8W', 'Giacomo Chaney'),
    ('24fe821e-8413-484c-ae63-d6695bff9083', 'ornare', '$2y$10$1UfTOZOv8dkFa46KBJMc5u0bj.iJ7G06mng7OLATiouX4FTXWiCgC', 'Isabelle Snider'),
    ('3dd7fc24-1d18-4d3a-90d8-0f797c3e460a', 'pede', '$2y$10$JDbGuRA3jDTNEvSS4g/vJefCwJJGJ.jcIC.FkwrEs8C4YfDOZEXzG', 'Serena Green'),
    ('317f9a07-0dac-4a5d-9d52-8e04cb323d2b', 'fermentum', '$2y$10$d9ZOIJ3fR2j273psDlQ2.O2DTjxvMoWzuHI/Pa.R6ADQAD1l8T0GS', 'Farrah Carver'),
    ('ff525adb-c000-454b-9683-9a51603b164f', 'id', '$2y$10$WT7IPhy3KMLXgz1yUnofy.Fpe7J4Y79D3Ku0O72ntGI3VM2QI.KB2', 'Sonia Case'),
    ('6ca9a82a-ae64-4560-ae6f-97ca0ac6cf3c', 'eu', '$2y$10$0VeoyGR5honV.4/q9dCaU.pTexxz91Aer46qvpZx2Qq/5b1XCagtK', 'Inga Skinner'),
    ('26015277-af00-4e2e-a762-ec2aaf55989f', 'malesuada', '$2y$10$6OXVyPVoofEnMUgPfCOga.3Oe9t0MHIEi1TcpVRh6arLf8jvFLf2C', 'Lee Colon'),
    ('fc488a46-c15f-4a34-a4af-9c9c432b3492', 'elit', '$2y$10$XHGCy5TKrg6V7lP0qItBxu/.DQP3BBOTZ3LzfS2qZ5Mz9xV5Q3kXy', 'Raja Herrera'),
    ('158b3e04-d870-4965-b8a3-678f6eb18cf1', 'condimentum', '$2y$10$rfjYy.Qy0XD/49.stJSIMe1uyHZZzU7BPDuaHntuY5NVde/Jvlmq2', 'Brenda Guzman'),
    ('4e401581-35c6-4543-8103-9c6266335c22', 'diam', '$2y$10$9U4A3X2O6ue2tO3qpIR8lOeaObVVx7Rjpc5KsEOqDexnkcThVBLG6', 'Maggy Calderon'),
    ('b478f973-1fb5-461c-86ce-664d9e8a2cc6', 'fringilla', '$2y$10$qTbyiX52S/i77so6i/Lb9OIv05bzebM3xxpLfYcTpMj.6oEfmyMZ.', 'Tanner Hays'),
    ('2ac4c08f-67b9-4cbf-95e9-ff0bffd1ce9b', 'Fusce', '$2y$10$zYIZAabBkyR6.XsDL3kN1ee7KJnBOQoI1NyCJUUnyYdiOAL6td3Qy', 'Wyoming Newton'),
    ('f3e45c8a-4d91-4eb0-b82b-7a80ddfdcde5', 'consectetuer', '$2y$10$B2b.RdK6jqmUdqGm6UlZWelv/PcBwCBCitwmRPP0fdCW4yotl3cTi', 'Daquan Melton'),
    ('35efca1d-8b83-44cc-9eb0-f7510fd33ce2', 'ante', '$2y$10$A3UcuYqVSJaKU5D55Py8aOwmi54RSNL4yVJ/9dJM4TogA2g9hfELO', 'Lisandra Shaffer'),
    ('0e4e3e40-0d28-44bc-a79e-aa997aa0a8c0', 'tempus', '$2y$10$T2HXe6J8fwVdKwIv9Tbvf.dQnTV2Bl0atG6FvbIbzSNE02h0W1HSC', 'Armando Pittman'),
    ('0b2b18b2-9fe7-406a-93a0-eddcf0dc9c09', 'rutrum', '$2y$10$a5sQ.IMOgOtmJzh5rMdx7uMvQ5wI1kd/XiWjKhTR5Z3ufklatZUWO', 'Dara Guthrie'),
    ('4702414a-c56d-452e-8a23-ad54ccb9c943', 'turpis', '$2y$10$r0slHirwAZe2ckqzclBLUeXl9RpQi0wKRmJEbYzSbfpzgi74kUYra', 'Yael Lowe'),
    ('80e03abe-fc7b-4d7b-b8dd-27913ede187e', 'nisl', '$2y$10$AVTA.PE84wyJImiSGqREg.skjOHTpgjJgdMjFYIv/uis9fmDmZq2y', 'Claudia Clayton'),
    ('156adf67-6c0f-4d15-978a-0f4015d81da3', 'montes', '$2y$10$A2.VfvwOmhG3dQgzOmSk1OoCGJlh3i1Ev14uMxfP.WqXbR3iLzSqu', 'Tanya Gardner'),
    ('73ed7c20-eebb-4fc5-a180-01a1113929d0', 'nulla', '$2y$10$3TUQZzzhHzQvrB8n8lFGAepzjt2SrIKynq7ha77aAFDHGFmxbCoQi', 'Lacy Maldonado'),
    ('622007a1-a207-4311-b861-ac09db17f089', 'ipsum', '$2y$10$JEmWG.jZ1yhsRqu5ts3O6uXn06oUj8vzRksRelW8AmCwEBoTZp98O', 'Baker Brennan'),
    ('bceb4ce8-6d37-467a-bad9-8f90de13f4a5', 'DeaconRoachvwr', '$2y$10$PDl88xrDETQhGeJ/lNdZJOnXGw.vvDVyy3jQYtA37dzaB/SFXS.da', 'Deacon Roach'),
    ('2c9b9264-c877-41a7-a66a-b03b144b01ec', 'Ut', '$2y$10$k.rHxKMh8xxOWHUm6itrMekyix1ElJv4/UtjV1V.exQoH6G.Kqqu6', 'Ann Montoya'),
    ('3435472b-5692-47eb-8997-4d9ccdfa0f65', 'SawyerCobbMBQ', '$2y$10$TeT/SV6orpYnRlMRZ4GvW.2bjZI1lB5KjrwQt0Zu6LmrZIBemWMku', 'Sawyer Cobb'),
    ('8d75a11e-84f8-4189-8b08-aa531ad8cf47', 'nisi', '$2y$10$/QW9j9ocnVshoHLz5x9A3OfYMwCSF0B7F1lu/XHdB9HaGwByEnQE2', 'Jane Osborn'),
    ('149d9a24-e0d5-4313-91ff-3401b82e4baa', 'consequat', '$2y$10$VeDdlWqeiVsWJV.5td4JCeDZV7BZm3mYhG8P5JfxMhPMHAPAI9.WO', 'Dean Bright'),
    ('c77ea65d-966e-424b-86fc-faa00d905104', 'IndiaMcdowellO06', '$2y$10$xkgsCVOEZHAxz.G2NR/6helNTQ2FAGDVovYGCCR7x.AkIL5DHR2GC', 'India Mcdowell'),
    ('85ae6eb5-f7de-44ed-88f2-a8ea09b3f2ea', 'BellBarrera8Xd', '$2y$10$K08v2gh6zJHeFMLYB3M1iObPMQWcNTrCGkoN1bCsfqY1r2XV8x0oS', 'Bell Barrera'),
    ('8e6caaaa-d630-4532-9f5a-de9467803950', 'Aenean', '$2y$10$dxXPoTGatOGdciSFw4BeLeIKzziZBx1LPr6QJgEYuIG0brzElZN7O', 'David Gomez'),
    ('6b9e78ad-dba0-4d3e-b6f6-4dbfc2bde116', 'FallonRobertsonvXb', '$2y$10$UPChbmvsTe8CWQRxtU3YS.5UoB18t8qgq0c.GLRrab3Z5E9iqzQpm', 'Fallon Robertson'),
    ('bf3c00c9-60d8-45df-8a5d-8d45e789dde6', 'RhodaWolfWtL', '$2y$10$flhi8UJIlpUWW4RjInUX5eR6rG4vpWdT1ukysxI9g5Jec.w8qCumy', 'Rhoda Wolf'),
    ('e807a71e-803d-424e-b842-bed4bf01ca9d', 'urna', '$2y$10$KF0NfUaw2Bl4i3ihkT827eIu//VnydXwb84Jmu8yaWnCY0cY8tY2W', 'Colette Ayala'),
    ('7f7a49d0-0294-40e2-8746-61a44cc37290', 'RoganDayevo', '$2y$10$kpRjTvD6IRZ0xPniIFx70.0MclKuPh7D1ShFSfbJD4txc8PKNlA6W', 'Rogan Day'),
    ('2a15644f-8c78-407b-b7c9-15a2d4550629', 'amet', '$2y$10$Nurc6Nn8EwfGRYyEo/sag.EnqosNRUmhrJhhHDAaS5J6dpdQIuVQO', 'Maisie Madden'),
    ('879a4c63-31f5-4458-a1f2-378b51774636', 'vel', '$2y$10$uHqWlgSaBQpJQ1vivBKZSubmvb7CCYJ2nQBkmQ.5J.o3TpjNSn0HS', 'Jacob Richardson'),
    ('56c6463d-6916-474f-91fe-6b05d1588fac', 'dui', '$2y$10$2sIXIo3MwZRkZNLbgywNmu3j5lNt.ENGpADKxzTp.aw1/ni6BPU26', 'Alexa Dunn'),
    ('726e899a-a8f3-43f7-86f2-b7c5916003ec', 'nec', '$2y$10$dd1XcUVY98ZnL1Ifx8YKQ.sfwDHTofmfXIqotrrzyu2W6jEHfd6MK', 'Cathleen Erickson'),
    ('b079d5d8-990b-488a-9256-106f1f9413ff', 'MartinaKellyo2W', '$2y$10$we5EoAlE4qZAD9iV1571Xu6jV54ql3X..hwwUdOvwmIhMAIh9C4x2', 'Martina Kelly'),
    ('126c084a-a204-4f79-9ff3-1efa3bde95a8', 'non', '$2y$10$Tlje6QFnnlLZYOnaFp56pubW7YKgqHnxQvhlDrRa3X//lQ3XOvS16', 'Ciara Frank'),
    ('d8901761-9f71-4a6e-918a-bfbd880fc80e', 'ChardeDickerson2Dk', '$2y$10$coOG2MADIhhIiX20ryPy2urxaO22WOgr0GoQFP9n/SELimxntDrcC', 'Charde Dickerson'),
    ('901e92a1-4284-4201-82f4-2355bd168172', 'Etiam', '$2y$10$.7IJ13l/.NKz45OAJbgGIOpSFzaQDydRvudNzow9DTvsKT4lcaqM.', 'Shaine Taylor'),
    ('e975d0a6-098d-4d48-936e-77836fd82b17', 'LaneBoothYrs', '$2y$10$fqnz2v/EYKwz3sZf9q7wcO5R5a4iuI/NPig9zP2tGlSQpCworZB6u', 'Lane Booth'),
    ('01e9a65b-6a34-4b41-a528-b5f013d57ee6', 'RosalynMortonGEi', '$2y$10$YEkQC2u.3IE3bmpTOzphkufKQXXbylBmXY29WBLTGNxwTmFNiqcwS', 'Rosalyn Morton'),
    ('19bf6b02-9205-4ede-ac89-13e2a9eae127', 'ShelleyLynchmDW', '$2y$10$fmOiLABh8hg5brjBxsKgyuqhFp.xtQ4kqDFbheAVg68A1GNayQq12', 'Shelley Lynch'),
    ('0c552dc5-b78b-4694-b57c-d6f86456e35b', 'a', '$2y$10$mJb.KCctaBc18AR8BvDtxuUbsq1OU9uwYXROwR.QVyjnqnQbnarrS', 'Amy Mcconnell'),
    ('8131b9f8-4608-4565-b53f-81f51c47ae34', 'JerryJohnstonaEA', '$2y$10$uSQ7yPKWh1um7TEBabW31eHa3TOUWF1ARph4x7QyQx8Y44m62x/i2', 'Jerry Johnston'),
    ('4304292b-2089-46bc-93b9-f1f7d275ad90', 'dignissim', '$2y$10$pbv6IodtcikGWCB0cd7f8u/gErPPqW0aEVkjLrcx71UZcPYDTIigC', 'Kaden Graham'),
    ('136a7fa1-2fce-4135-b5a5-b3549ce75d39', 'KaliaCarneyTJx', '$2y$10$.JvcnvQRmhb2auGZgKlzrueAMea6pKTb1vuiaGte.apd8U81shb7i', 'Kalia Carney'),
    ('c25935bc-0b49-48b0-bbe8-b4538ec442e2', 'HarrietGalloway1Yx', '$2y$10$iw/1INgLoYkDagZSEl2d/Ozp01zc35H/D3p3hLpChC9OUKI1RKNtK', 'Harriet Galloway'),
    ('a97475da-f121-428a-ae29-837c2cafd7b7', 'DoraHensonXjK', '$2y$10$KdGXjFLitoKqH11PgbTNLeTgjapskydZR135SfcOKGqYwZd0YXQCm', 'Dora Henson'),
    ('1d555c00-9a4b-47bb-8e66-ef4a08554256', 'Phasellus', '$2y$10$hR4IHRxgQ/FP.YqYQ0di3uuVpynZIZwk48tUujxkAHdEboUqG4FdS', 'Iona Lowe'),
    ('78b54231-dc4a-406b-a09d-e2c3a014a051', 'SukiMckenzie4jr', '$2y$10$OrFyDpd7DvSZBZycCplYnuko7IFA8OiDJcaLyoQrVK9P4N0gzgupm', 'Suki Mckenzie'),
    ('553fbf9e-da95-4a6f-bc01-dfdd67e219c9', 'KyraHarding87W', '$2y$10$ceeVJZoViy7.BsMh7gruauptIs1MUZXO8Mih5xURnXTs40MgRrb1y', 'Kyra Harding'),
    ('c746bee8-56b2-44f5-bef5-28418ea39b31', 'MaileMossUtc', '$2y$10$FTZKUgrLJXxQpsLeDsHRq.QxL7dNSodmlF9c7Ruo7TzDmT55l9gl.', 'Maile Moss'),
    ('0758404f-075e-4331-b546-3bd3718358e7', 'eget', '$2y$10$lBsr/FHDFoGhBn9LivAPO.GRq2evZM3L0L7YoG9azP7Dj92qUEqeO', 'Bertha Scott'),
    ('33816c03-b007-4cea-9d9a-53b9a4622335', 'MeredithBurkeZg6', '$2y$10$b1NPz263T6aomiD4mGK60e5EbVo3JSPkdO9FVYMBzg9KKEEgSVnma', 'Meredith Burke'),
    ('a0b9e3ba-ddaa-4e89-a65a-b53f6b55c667', 'mi', '$2y$10$JQ2rXtburmG//DLUDQiA6..CK8dS.UHfFesVOBarcS2eEZuHpWyL2', 'Phoebe Hudson'),
    ('c15bf646-5ce8-45b7-9dbc-baf7084eb8f3', 'enim', '$2y$10$Fw/VAHAH77k8g4zj00mb8Os.RbNIPQREUGbglXcNaBu24jyJRmrZm', 'Thor Ramos'),
    ('65b582bb-010d-418b-b132-ba45ac005505', 'arcu', '$2y$10$D2dYY2ZOiDyDvaEnxHiFSubQRrln9JJU4GM/3UH8l7ufMKjFkzCny', 'Cody Simon'),
    ('1ce58ae3-a29b-405b-a42d-f724ed94ff58', 'lectus', '$2y$10$nueR6cjTxC.y9UbrvOA1eeA5Kjhd5kdSMR31BnCUC262yuRUlVpZO', 'Jelani Flowers'),
    ('fec417a5-f200-4a8c-8d5d-2291dcec7ffb', 'Quisque', '$2y$10$g6GZN2lAIvd9cXlIsaTx2.hwOeF2qMs0GDWlX.F.MPMgQu.pfb1wa', 'Clementine Stone'),
    ('158c9b61-353f-4a56-8353-ce5366178d68', 'AllistairChristenseno5G', '$2y$10$1h9UBg5NxOrasZ0SLyg9VekFI/zdZiRS9Rw/eF/9rd.8uPWm1U6ai', 'Allistair Christensen'),
    ('747288dd-1ab5-4f38-8b56-64dda3b7ae20', 'XanthaFrank6kS', '$2y$10$8EXl1.jEfqGE7iqArOPkH.xoosLJa/9lL5Q3vhiJs3LzrZuIipPxq', 'Xantha Frank'),
    ('ed342cbd-8c29-4463-bb99-18438d36cd5a', 'VictoriaHorneISU', '$2y$10$ckUx1G9BsaOmHu8L1ya.ZeWXK45F9WZIzVkMH05H6uHu9umdJYeNi', 'Victoria Horne'),
    ('3f813b37-760d-4b91-8695-be6b1ace7fab', 'WingRush5qV', '$2y$10$qGBl0KtBDdOpfZNHSYe2tOlcO0JDi/l9c30ijSNsO8hbMF/umDbUm', 'Wing Rush'),
    ('9918cf1a-290e-4e9c-b1f9-3c8276927a7c', 'FrancesWebster8Ce', '$2y$10$T4.5jYIstHni6zHWgI1L8OQHP0v.18kb.Fhy3xxTwokewEkKPc/le', 'Frances Webster'),
    ('155d8c1a-758a-499c-bf22-d42db11437cf', 'lacus', '$2y$10$K6tiIhYCjbnYra2PD0avC.H2AgFJ5nOMQaqHA5qTYqeMwZULcoSG2', 'Mollie Young'),
    ('9c30d5df-1211-42b5-a9af-c4386430e6d5', 'pretium', '$2y$10$ZFUKqc5/ikSZMA2yAztfuOcomZ42YEbDnxnC9yiYeOWwXPVanNKki', 'Yoshio Villarreal'),
    ('d00b7532-bc5c-487e-bb43-1944d7609e9b', 'CandiceGuthriePpm', '$2y$10$fsiXVe8wvg9xM6ptBL2hWeEcg5LSFrrE8KEAPczbwUb9DedZv7v0y', 'Candice Guthrie'),
    ('9879fa1b-7939-43fb-9e85-dd399e94a243', 'CassidyNguyenwe3', '$2y$10$RGHQQzUbnUahsn1HK1ThVec4muW27HMDQDCRlv583DQk54W0nXcSS', 'Cassidy Nguyen'),
    ('63bc3fa7-64ed-4cda-a800-2fffaf2aa0f4', 'ShelbySimmonso0e', '$2y$10$CzcJSyF5lOqW8bTbfrdwueINeUpLt3s2Y8ngO5skl20TKcABy7.zC', 'Shelby Simmons'),
    ('fcc51404-bf1a-4dde-a365-58177474ba2b', 'DanaJensenUFM', '$2y$10$9B4zXlb1DA01rInFBEsUwekbW3hjfeGr8orLxUEklqJY3IQ01Ln0S', 'Dana Jensen'),
    ('3ea1eb06-426d-46d8-a2eb-c22241817002', 'erat', '$2y$10$0fIojsUM7UhGFM.He7IPk.F2Yyh40FjUNtNMSx9JW.jT/An35J.LS', 'Cathleen Estrada');