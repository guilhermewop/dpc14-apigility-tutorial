<?php
return array(
    'Todo\\V1\\Rest\\Users\\Controller' => array(
        'collection' => array(
            'GET' => array(
                'description' => 'Retrieve paginated lists of users.',
                'request' => null,
                'response' => '{
   "_links": {
       "self": {
           "href": "/users"
       },
       "first": {
           "href": "/users?page={page}"
       },
       "prev": {
           "href": "/users?page={page}"
       },
       "next": {
           "href": "/users?page={page}"
       },
       "last": {
           "href": "/users?page={page}"
       }
   }
   "_embedded": {
       "users": [
           {
               "_links": {
                   "self": {
                       "href": "/users[/:users_id]"
                   }
               }
              "username": "Username (their email address)",
              "password": "Password to use for this user.",
              "name": "The user\'s full name."
           }
       ]
   }
}',
            ),
            'POST' => array(
                'description' => 'Create a new user.',
                'request' => '{
   "username": "Username (their email address)",
   "password": "Password to use for this user.",
   "name": "The user\'s full name."
}',
                'response' => '{
   "_links": {
       "self": {
           "href": "/users[/:users_id]"
       }
   }
   "username": "Username (their email address)",
   "password": "Password to use for this user.",
   "name": "The user\'s full name."
}',
            ),
            'description' => 'Manipulate users.',
        ),
        'entity' => array(
            'GET' => array(
                'description' => 'Retrieve a single user.',
                'request' => null,
                'response' => '{
   "_links": {
       "self": {
           "href": "/users[/:users_id]"
       }
   }
   "username": "Username (their email address)",
   "password": "Password to use for this user.",
   "name": "The user\'s full name."
}',
            ),
            'description' => 'Get information on individual users.',
        ),
        'description' => 'Create and retrieve user information.',
    ),
    'Todo\\V1\\Rest\\Lists\\Controller' => array(
        'collection' => array(
            'GET' => array(
                'description' => 'Retrieve a paginated set of todo lists.',
                'request' => null,
                'response' => '{
   "_links": {
       "self": {
           "href": "/lists"
       },
       "first": {
           "href": "/lists?page={page}"
       },
       "prev": {
           "href": "/lists?page={page}"
       },
       "next": {
           "href": "/lists?page={page}"
       },
       "last": {
           "href": "/lists?page={page}"
       }
   }
   "_embedded": {
       "lists": [
           {
               "_links": {
                   "self": {
                       "href": "/lists[/:lists_id]"
                   }
               }
              "title": "The title of the todo list."
           }
       ]
   }
}',
            ),
            'POST' => array(
                'description' => 'Create a todo list.',
                'request' => '{
   "title": "The title of the todo list."
}',
                'response' => '{
   "_links": {
       "self": {
           "href": "/lists[/:lists_id]"
       }
   }
   "title": "The title of the todo list."
}',
            ),
            'description' => 'Retrieve and create todo lists.',
        ),
        'entity' => array(
            'GET' => array(
                'description' => 'Retrieve a todo list.',
                'request' => null,
                'response' => '{
   "_links": {
       "self": {
           "href": "/lists[/:lists_id]"
       }
   }
   "title": "The title of the todo list."
}',
            ),
            'PUT' => array(
                'description' => 'Replace a todo list.',
                'request' => '{
   "title": "The title of the todo list."
}',
                'response' => '{
   "_links": {
       "self": {
           "href": "/lists[/:lists_id]"
       }
   }
   "title": "The title of the todo list."
}',
            ),
            'DELETE' => array(
                'description' => 'Delete a todo list.',
                'request' => null,
                'response' => null,
            ),
            'description' => 'Manipulate individual todo lists.',
        ),
        'description' => 'Manipulate and retrieve todo lists.',
    ),
    'Todo\\V2\\Rest\\Users\\Controller' => array(
        'collection' => array(
            'GET' => array(
                'description' => 'Retrieve paginated lists of users.',
                'request' => '',
                'response' => '{
   "_links": {
       "self": {
           "href": "/users"
       },
       "first": {
           "href": "/users?page={page}"
       },
       "prev": {
           "href": "/users?page={page}"
       },
       "next": {
           "href": "/users?page={page}"
       },
       "last": {
           "href": "/users?page={page}"
       }
   }
   "_embedded": {
       "users": [
           {
               "_links": {
                   "self": {
                       "href": "/users[/:users_id]"
                   }
               }
              "username": "Username (their email address)",
              "password": "Password to use for this user.",
              "name": "The user\'s full name."
           }
       ]
   }
}',
            ),
            'POST' => array(
                'description' => 'Create a new user.',
                'request' => '{
   "username": "Username (their email address)",
   "password": "Password to use for this user.",
   "name": "The user\'s full name."
}',
                'response' => '{
   "_links": {
       "self": {
           "href": "/users[/:users_id]"
       }
   }
   "username": "Username (their email address)",
   "password": "Password to use for this user.",
   "name": "The user\'s full name."
}',
            ),
            'description' => 'Manipulate users.',
        ),
        'entity' => array(
            'GET' => array(
                'description' => 'Retrieve a single user.',
                'request' => '',
                'response' => '{
   "_links": {
       "self": {
           "href": "/users[/:users_id]"
       }
   }
   "username": "Username (their email address)",
   "password": "Password to use for this user.",
   "name": "The user\'s full name."
}',
            ),
            'description' => 'Get information on individual users.',
        ),
        'description' => 'Create and retrieve user information.',
    ),
    'Todo\\V2\\Rest\\Lists\\Controller' => array(
        'collection' => array(
            'GET' => array(
                'description' => 'Retrieve a paginated set of todo lists.',
                'request' => '',
                'response' => '{
   "_links": {
       "self": {
           "href": "/lists"
       },
       "first": {
           "href": "/lists?page={page}"
       },
       "prev": {
           "href": "/lists?page={page}"
       },
       "next": {
           "href": "/lists?page={page}"
       },
       "last": {
           "href": "/lists?page={page}"
       }
   }
   "_embedded": {
       "lists": [
           {
               "_links": {
                   "self": {
                       "href": "/lists[/:lists_id]"
                   }
               }
              "title": "The title of the todo list."
           }
       ]
   }
}',
            ),
            'POST' => array(
                'description' => 'Create a todo list.',
                'request' => '{
   "title": "The title of the todo list."
}',
                'response' => '{
   "_links": {
       "self": {
           "href": "/lists[/:lists_id]"
       }
   }
   "title": "The title of the todo list."
}',
            ),
            'description' => 'Retrieve and create todo lists.',
        ),
        'entity' => array(
            'GET' => array(
                'description' => 'Retrieve a todo list.',
                'request' => '',
                'response' => '{
   "_links": {
       "self": {
           "href": "/lists[/:lists_id]"
       }
   }
   "title": "The title of the todo list."
}',
            ),
            'PUT' => array(
                'description' => 'Replace a todo list.',
                'request' => '{
   "title": "The title of the todo list."
}',
                'response' => '{
   "_links": {
       "self": {
           "href": "/lists[/:lists_id]"
       }
   }
   "title": "The title of the todo list."
}',
            ),
            'DELETE' => array(
                'description' => 'Delete a todo list.',
                'request' => '',
                'response' => '',
            ),
            'description' => 'Manipulate individual todo lists.',
        ),
        'description' => 'Manipulate and retrieve todo lists.',
    ),
    'Todo\\V2\\Rest\\UserLists\\Controller' => array(
        'collection' => array(
            'GET' => array(
                'description' => 'Retrieve a paginated set of the user\'s todo lists.',
                'request' => null,
                'response' => '{
   "_links": {
       "self": {
           "href": "/users/:user_id/lists"
       },
       "first": {
           "href": "/users/:user_id/lists?page={page}"
       },
       "prev": {
           "href": "/users/:user_id/lists?page={page}"
       },
       "next": {
           "href": "/users/:user_id/lists?page={page}"
       },
       "last": {
           "href": "/users/:user_id/lists?page={page}"
       }
   }
   "_embedded": {
       "lists": [
           {
               "_links": {
                   "self": {
                       "href": "/users/:user_id/lists"
                   }
               }
              "title": "Title for the todo list."
           }
       ]
   }
}',
            ),
            'POST' => array(
                'description' => 'Create a new todo list.',
                'request' => '{
   "title": "Title for the todo list."
}',
                'response' => '{
   "_links": {
       "self": {
           "href": "/users/:user_id/lists"
       }
   }
   "title": "Title for the todo list."
}',
            ),
            'description' => 'Work with a user\'s lists.',
        ),
        'description' => 'Retrieve and create lists for a user.',
    ),
    'Todo\\V2\\Rest\\Tasks\\Controller' => array(
        'collection' => array(
            'GET' => array(
                'description' => 'Retrieve a paginated list of tasks for a todo list.',
                'request' => null,
                'response' => '{
   "_links": {
       "self": {
           "href": "/lists/:lists_id/tasks"
       },
       "first": {
           "href": "/lists/:lists_id/tasks?page={page}"
       },
       "prev": {
           "href": "/lists/:lists_id/tasks?page={page}"
       },
       "next": {
           "href": "/lists/:lists_id/tasks?page={page}"
       },
       "last": {
           "href": "/lists/:lists_id/tasks?page={page}"
       }
   }
   "_embedded": {
       "tasks": [
           {
               "_links": {
                   "self": {
                       "href": "/lists/:lists_id/tasks[/:tasks_id]"
                   }
               }
              "name": "The task name.",
              "completed": "Status of the task."
           }
       ]
   }
}',
            ),
            'POST' => array(
                'description' => 'Create a new task.',
                'request' => '{
   "name": "The task name.",
   "completed": "Status of the task."
}',
                'response' => '{
   "_links": {
       "self": {
           "href": "/lists/:lists_id/tasks[/:tasks_id]"
       }
   }
   "name": "The task name.",
   "completed": "Status of the task."
}',
            ),
            'description' => 'Display and create tasks.',
        ),
        'entity' => array(
            'PATCH' => array(
                'description' => 'Modify a task',
                'request' => '{
   "name": "The task name.",
   "completed": "Status of the task."
}',
                'response' => '{
   "_links": {
       "self": {
           "href": "/lists/:lists_id/tasks[/:tasks_id]"
       }
   }
   "name": "The task name.",
   "completed": "Status of the task."
}',
            ),
            'DELETE' => array(
                'description' => 'Delete a task.',
                'request' => null,
                'response' => null,
            ),
            'description' => 'Manipulate tasks.',
        ),
        'description' => 'Manipulate tasks for todo lists.',
    ),
    'Todo\\V2\\Rest\\ListUsers\\Controller' => array(
        'collection' => array(
            'GET' => array(
                'description' => 'Retrieve a paginated set of users with access to the list.',
                'request' => null,
                'response' => '{
   "_links": {
       "self": {
           "href": "/lists/:lists_id/users"
       },
       "first": {
           "href": "/lists/:lists_id/users?page={page}"
       },
       "prev": {
           "href": "/lists/:lists_id/users?page={page}"
       },
       "next": {
           "href": "/lists/:lists_id/users?page={page}"
       },
       "last": {
           "href": "/lists/:lists_id/users?page={page}"
       }
   }
   "_embedded": {
       "users": [
           {
               "_links": {
                   "self": {
                       "href": "/lists/:lists_id/users[/:users_id]"
                   }
               }
              "can_write": "Is the user allowed to write to the list?",
              "can_read": "Is the user allowed read-access to the list?"
           }
       ]
   }
}',
            ),
            'POST' => array(
                'description' => 'Add a user to the list.',
                'request' => '{
   "user_id": "User identifier of the person to add to the list (optional; one of this or username must be present)",
    "username": "Username (email) of the person to add to the list (optional; one of this or user_id must be present)",
   "can_write": "Is the user allowed to write to the list?",
   "can_read": "Is the user allowed read-access to the list?"
}',
                'response' => '{
   "_links": {
       "self": {
           "href": "/lists/:lists_id/users[/:users_id]"
       }
   }
   "can_write": "Is the user allowed to write to the list?",
   "can_read": "Is the user allowed read-access to the list?"
}',
            ),
            'description' => 'Retrieve users associated with the list, or add users to the list.',
        ),
        'entity' => array(
            'PATCH' => array(
                'description' => 'Update user permissions.',
                'request' => '{
   "can_write": "Is the user allowed to write to the list?",
   "can_read": "Is the user allowed read-access to the list?"
}',
                'response' => '{
   "_links": {
       "self": {
           "href": "/lists/:lists_id/users[/:users_id]"
       }
   }
   "can_write": "Is the user allowed to write to the list?",
   "can_read": "Is the user allowed read-access to the list?"
}',
            ),
            'DELETE' => array(
                'description' => 'Remove a user\'s access to the list.',
                'request' => null,
                'response' => null,
            ),
            'GET' => array(
                'description' => 'Retrieve a user\'s permissions to the list.',
                'request' => null,
                'response' => '{
   "_links": {
       "self": {
           "href": "/lists/:lists_id/users[/:users_id]"
       }
   }
   "can_write": "Is the user allowed to write to the list?",
   "can_read": "Is the user allowed read-access to the list?"
}',
            ),
            'description' => 'Manipulate user permissions to the list.',
        ),
        'description' => 'Manipulate user access to a list.',
    ),
);
