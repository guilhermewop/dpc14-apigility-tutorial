<?php
namespace Todo\V1\Rest\Lists;

use XTilDone\Lists\MapperInterface as ListsMapper;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

class ListsResource extends AbstractResourceListener
{
    protected $mapper;

    public function __construct(ListsMapper $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        $data     = $this->getInputFilter()->getValues();
        $identity = $this->getIdentity();
        return $this->mapper->create($identity->getRoleId(), $data['title']);
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        $identity = $this->getIdentity();
        return $this->mapper->delete($identity->getRoleId(), $id);
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        $identity = $this->getIdentity();
        return $this->mapper->fetch($identity->getRoleId(), $id);
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = array())
    {
        $identity = $this->getIdentity();
        return $this->mapper->fetchAll($identity->getRoleId());
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        $data     = $this->getInputFilter()->getValues();
        $identity = $this->getIdentity();
        return $this->mapper->update($identity->getRoleId(), $id, $data['title']);
    }
}
