<?php

namespace Todo\V2\Rest\Lists;

use XTilDone\Lists\TableGatewayMapper;

class TableGatewayMapperFactory
{
    public function __invoke($services)
    {

        return new TableGatewayMapper(
            $services->get(__NAMESPACE__ . '\TableGateway'),
            $services->get('Todo\V2\Rest\UserLists\TableGateway'),
            __NAMESPACE__ . '\ListsEntity',
            __NAMESPACE__ . '\ListsCollection'
        );
    }
}
