<?php
namespace Todo\V2\Rest\Lists;

use ArrayObject;

class ListsEntity extends ArrayObject
{
    public function getArrayCopy()
    {
        return array(
            'list_id' => $this['list_id'],
            'title'   => $this['title'],
        );
    }
}
