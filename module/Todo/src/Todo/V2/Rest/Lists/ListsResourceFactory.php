<?php
namespace Todo\V2\Rest\Lists;

class ListsResourceFactory
{
    public function __invoke($services)
    {
        return new ListsResource($services->get(__NAMESPACE__ . '\Mapper'));
    }
}
