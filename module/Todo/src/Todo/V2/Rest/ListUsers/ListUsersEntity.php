<?php
namespace Todo\V2\Rest\ListUsers;

use ArrayObject;

class ListUsersEntity extends ArrayObject
{
    public function getArrayCopy()
    {
        $isOwner  = isset($this['is_owner'])  ? (int) $this['is_owner']  : 0;
        $canWrite = isset($this['can_write']) ? (int) $this['can_write'] : 0;
        $canRead  = isset($this['can_read'])  ? (int) $this['can_read']  : 0;

        return array(
            'user_id'   => $this['user_id'],
            'is_owner'  => $isOwner,
            'can_write' => ($isOwner || $canWrite) ? 1 : 0,
            'can_read'  => ($isOwner || $canWrite || $canRead) ? 1 : 0,
        );
    }
}
