<?php

namespace Todo\V2\Rest\ListUsers;

use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Stdlib\Hydrator\ArraySerializable;

class TableGatewayFactory
{
    public function __invoke($services)
    {
        $resultSetPrototype = new HydratingResultSet(
            new ArraySerializable(),
            new ListUsersEntity()
        );
        return new TableGateway(
            'user_list',
            $services->get('Db\Todo'),
            null,
            $resultSetPrototype
        );
    }
}
