<?php

namespace Todo\V2\Rest\ListUsers;

use XTilDone\ListUsers\TableGatewayMapper;

class TableGatewayMapperFactory
{
    public function __invoke($services)
    {
        return new TableGatewayMapper(
            $services->get(__NAMESPACE__ . '\TableGateway'),
            $services->get('Todo\V2\Rest\Lists\Mapper'),
            $services->get('Todo\V2\Rest\Users\Mapper'),
            __NAMESPACE__ . '\ListUsersEntity',
            __NAMESPACE__ . '\ListUsersCollection'
        );
    }
}
