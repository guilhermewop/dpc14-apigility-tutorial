<?php
namespace Todo\V2\Rest\Users;

class UsersResourceFactory
{
    public function __invoke($services)
    {
        return new UsersResource($services->get(__NAMESPACE__ . '\Mapper'));
    }
}
