<?php

namespace Todo\V2\Rest\Users;

use XTilDone\Users\TableGatewayMapper;

class TableGatewayMapperFactory
{
    public function __invoke($services)
    {
        return new TableGatewayMapper(
            $services->get('Todo\V2\Rest\Users\TableGateway'),
            __NAMESPACE__ . '\UsersEntity',
            __NAMESPACE__ . '\UsersCollection'
        );
    }
}
