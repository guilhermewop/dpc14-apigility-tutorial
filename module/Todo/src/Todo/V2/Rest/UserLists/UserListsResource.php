<?php
namespace Todo\V2\Rest\UserLists;

use XTilDone\Lists\MapperInterface as ListMapper;
use ZF\ApiProblem\ApiProblem;
use ZF\Hal\Entity as HalEntity;
use ZF\Hal\Link\Link;
use ZF\Rest\AbstractResourceListener;

class UserListsResource extends AbstractResourceListener
{
    protected $lists;

    public function __construct(ListMapper $lists)
    {
        $this->lists = $lists;
    }

    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return HalEntity
     */
    public function create($data)
    {
        $data = $this->getInputFilter()->getValues();
        $identity = $this->getIdentity();

        $list = $this->lists->create($identity->getRoleId(), $data['title']);
        $halEntity = new HalEntity($list, $list['list_id']);
        $halEntity->getLinks()->add(Link::factory(array(
            'rel' => 'self',
            'route' => array(
                'name' => 'todo.rest.lists',
                'params' => array(
                    'lists_id' => $list['list_id'],
                ),
            ),
        )));
        return $halEntity;
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        return new ApiProblem(405, 'The GET method has not been defined for individual resources');
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return \Todo\V2\Rest\Lists\ListsCollection
     */
    public function fetchAll($params = array())
    {
        $identity = $this->getIdentity();
        $collection = $this->lists->fetchAll($identity->getRoleId());
        return ($collection->count() ? $collection : array());
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }
}
