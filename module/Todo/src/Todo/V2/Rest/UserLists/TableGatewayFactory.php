<?php

namespace Todo\V2\Rest\UserLists;

use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Stdlib\Hydrator\ArraySerializable;

class TableGatewayFactory
{
    public function __invoke($services)
    {
        return new TableGateway('user_list', $services->get('Db\Todo'));
    }
}
