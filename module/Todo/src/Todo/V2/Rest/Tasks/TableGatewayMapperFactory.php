<?php

namespace Todo\V2\Rest\Tasks;

use XTilDone\Tasks\TableGatewayMapper;

class TableGatewayMapperFactory
{
    public function __invoke($services)
    {
        return new TableGatewayMapper(
            $services->get(__NAMESPACE__ . '\TableGateway'),
            $services->get('Todo\V2\Rest\Lists\Mapper'),
            'user_list', // This could be pulled from configuration if desired
            __NAMESPACE__ . '\TasksEntity',
            __NAMESPACE__ . '\TasksCollection'
        );
    }
}
