<?php
namespace Todo\V2\Rest\Tasks;

use XTilDone\Tasks\MapperInterface as TaskMapper;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

class TasksResource extends AbstractResourceListener
{
    protected $mapper;

    public function __construct(TaskMapper $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        $data     = $this->getInputFilter()->getValues();
        $identity = $this->getIdentity();
        $userId   = $identity->getRoleId();
        $listId   = $this->getEvent()->getRouteParam('lists_id', false);

        if (! $listId) {
            return new ApiProblem(400, 'Missing list identifier; cannot create task');
        }

        return $this->mapper->create($userId, $listId, $data['name']);
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        $identity = $this->getIdentity();
        $userId   = $identity->getRoleId();
        $listId   = $this->getEvent()->getRouteParam('lists_id', false);

        if (! $listId) {
            return new ApiProblem(404, 'Missing list identifier; cannot delete task; task not found');
        }

        return $this->mapper->delete($userId, $listId, $id);
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        return new ApiProblem(405, 'The GET method has not been defined for individual resources');
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = array())
    {
        $identity = $this->getIdentity();
        $userId   = $identity->getRoleId();
        $listId   = $this->getEvent()->getRouteParam('lists_id', false);

        if (! $listId) {
            return new ApiProblem(404, 'Missing list identifier; tasks not found');
        }

        return $this->mapper->fetchAll($userId, $listId);
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        $data     = $this->getInputFilter()->getValues();
        $identity = $this->getIdentity();
        $userId   = $identity->getRoleId();
        $listId   = $this->getEvent()->getRouteParam('lists_id', false);

        if (! $listId) {
            return new ApiProblem(404, 'Missing list identifier; task not found');
        }

        return $this->mapper->update($userId, $listId, $id, $data);
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }
}
