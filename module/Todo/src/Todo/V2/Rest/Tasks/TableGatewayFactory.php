<?php

namespace Todo\V2\Rest\Tasks;

use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Stdlib\Hydrator\ArraySerializable;

class TableGatewayFactory
{
    public function __invoke($services)
    {
        $resultSetPrototype = new HydratingResultSet(new ArraySerializable(), new TasksEntity());
        return new TableGateway('task', $services->get('Db\Todo'), null, $resultSetPrototype);
    }
}
