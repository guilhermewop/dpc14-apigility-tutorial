<?php
namespace Todo;

use Zend\EventManager\SharedEventManagerInterface;
use Zend\EventManager\SharedListenerAggregateInterface;
use ZF\Hal\Entity as HalEntity;
use ZF\Hal\Link\Link;

class LinkInjectorListener implements SharedListenerAggregateInterface
{
    protected $listeners = array();

    protected $entityRegexTemplate;

    public function __construct()
    {
        $ns = preg_quote('\\');
        $this->entityRegexTemplate = '/^Todo' . $ns . '+V(?P<version>\d+)' . $ns . '+.*?' . $ns . '%sEntity$/';
    }

    /**
     * Attach events to the HAL plugin
     *
     * This method attaches listeners to the renderEntity and renderCollection.entity
     * events of ZF\Hal\Plugin\Hal.
     * 
     * @param SharedEventManagerInterface $events 
     */
    public function attachShared(SharedEventManagerInterface $events)
    {
        $this->listeners = $events->attach(
            'ZF\Hal\Plugin\Hal',
            'renderEntity',
            array($this, 'onRenderEntity')
        );

        $this->listeners = $events->attach(
            'ZF\Hal\Plugin\Hal',
            'renderCollection.entity',
            array($this, 'onRenderCollectionEntity')
        );
    }

    /**
     * Detach events from the shared event manager
     *
     * This method detaches listeners it has previously attached.
     * 
     * @param SharedEventManagerInterface $events 
     */
    public function detachShared(SharedEventManagerInterface $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach('ZF\Hal\Plugin\Hal', $listener)) {
                unset($listener[$index]);
            }
        }
    }

    /**
     * Listener for the "renderEntity" event
     *
     * @param \Zend\EventManager\Event $e 
     */
    public function onRenderEntity($e)
    {
        $halEntity = $e->getParam('entity');

        if ($this->isEntityOfInterest($halEntity->entity, 'Lists', 2)) {
            $this->injectListEntityLinks($halEntity);
            return;
        }

        if ($this->isEntityOfInterest($halEntity->entity, 'Users', 2)) {
            $this->injectUserEntityLinks($halEntity);
            return;
        }

        if ($this->isEntityOfInterest($halEntity->entity, 'ListUsers', 2)) {
            $this->injectListUsersEntityLinks($halEntity);
            return;
        }
    }

    /**
     * Listener for the "renderCollection.entity" event
     *
     * @param mixed $e 
     * @return void
     */
    public function onRenderCollectionEntity($e)
    {
        $entity = $e->getParam('entity');

        if ($this->isEntityOfInterest($entity, 'Lists', 2)) {
            $halEntity = new HalEntity($entity, $entity['list_id']);
            $this->injectListEntityLinks($halEntity);
            $e->setParam('entity', $halEntity);
            return;
        }

        if ($this->isEntityOfInterest($entity, 'Users', 2)) {
            $halEntity = new HalEntity($entity, $entity['user_id']);
            $this->injectUserEntityLinks($halEntity);
            $e->setParam('entity', $halEntity);
            return;
        }

        if ($this->isEntityOfInterest($entity, 'ListUsers', 2)) {
            $halEntity = new HalEntity($entity, $entity['user_id']);
            $this->injectListUsersEntityLinks($halEntity);
            $e->setParam('entity', $halEntity);
            return;
        }
    }

    /**
     * Determine if an entity is of interest (e.g., needs links or url parameter injection)
     *
     * The goal of this method is to test an entity to see if it is of a given
     * type and if the current entity version is equal to or greater than a
     * given mimimum version.
     *
     * The method accepts the entity, the "type" to check against (essentially
     * the service name), and the minimum version we're interested in.
     *
     * We're taking this approach to allow re-use of the method in later
     * exercises.
     *
     * You can use the protected member $entityRegexTemplate as a template for the 
     * regex to use; pass it to sprintf(), and provide the type; make sure you 
     * capture the matches to allow you to test the version!
     * 
     * @param mixed $entity 
     * @param string $type 
     * @param int $minVersion 
     * @return bool
     */
    protected function isEntityOfInterest($entity, $type, $minVersion)
    {
        if (! is_object($entity)) {
            return false;
        }

        $regex = sprintf($this->entityRegexTemplate, $type);

        if (! preg_match($regex, get_class($entity), $matches)) {
            return false;
        }

        if ($matches['version'] < $minVersion) {
            return false;
        }

        return true;
    }

    /**
     * Inject relational links into the List entity
     *
     * This method accepts a ZF\Hal\Entity instance composing a ListsEntity;
     * your job is to inject each of the following relational links if they 
     * have not yet been:
     *
     * - "self"
     * - "tasks"
     * - "users"
     * 
     * @param HalEntity $halEntity 
     */
    protected function injectListEntityLinks(HalEntity $halEntity)
    {
        $entity = $halEntity->entity;
        $listId = $entity['list_id'];
        $links  = $halEntity->getLinks();

        if (! $links->has('self')) {
            $links->add(Link::factory(array(
                'rel' => 'self',
                'route' => array(
                    'name' => 'todo.rest.lists',
                    'params' => array(
                        'lists_id' => $listId,
                    ),
                ),
            )));
        }

        if (! $links->has('tasks')) {
            $links->add(Link::factory(array(
                'rel' => 'tasks',
                'route' => array(
                    'name' => 'todo.rest.tasks',
                    'params' => array(
                        'lists_id' => $listId,
                    ),
                ),
            )));
        }

        if (! $links->has('users')) {
            $links->add(Link::factory(array(
                'rel' => 'users',
                'route' => array(
                    'name' => 'todo.rest.list-users',
                    'params' => array(
                        'lists_id' => $listId,
                    ),
                    'options' => array(
                        'reuse_matched_params' => false,
                    ),
                ),
            )));
        }
    }

    /**
     * Inject relational links into the User entity
     *
     * This method accepts a ZF\Hal\Entity instance composing a UsersEntity; 
     * your job is to inject each of the following relational links if they 
     * have not yet been:
     *
     * - "self"
     * - "lists"
     *
     * @param HalEntity
     */
    protected function injectUserEntityLinks(HalEntity $halEntity)
    {
        $entity = $halEntity->entity;
        $userId = $entity['user_id'];
        $links  = $halEntity->getLinks();

        if (! $links->has('self')) {
            $links->add(Link::factory(array(
                'rel' => 'self',
                'route' => array(
                    'name' => 'todo.rest.users',
                    'params' => array(
                        'users_id' => $userId,
                    ),
                ),
            )));
        }

        if (! $links->has('lists')) {
            $links->add(Link::factory(array(
                'rel' => 'lists',
                'route' => array(
                    'name' => 'todo.rest.user-lists',
                    'params' => array(
                        'user_id' => $userId,
                    ),
                ),
            )));
        }
    }


    /**
     * Inject relational links into the ListUser entity
     *
     * - "self"
     * - "user"
     *
     * @param HalEntity
     */
    protected function injectListUsersEntityLinks(HalEntity $halEntity)
    {
        $entity = $halEntity->entity;
        $userId = $entity['user_id'];
        $links  = $halEntity->getLinks();

        if (! $links->has('self')) {
            $links->add(Link::factory(array(
                'rel' => 'self',
                'route' => array(
                    'name' => 'todo.rest.list-users',
                    'params' => array(
                        'users_id' => $userId,
                    ),
                ),
            )));
        }

        if (! $links->has('user')) {
            $links->add(Link::factory(array(
                'rel' => 'user',
                'route' => array(
                    'name' => 'todo.rest.users',
                    'params' => array(
                        'users_id' => $userId,
                    ),
                ),
            )));
        }
    }
}
