<?php
return array(
    'db' => array(
        'adapters' => array(
            'Db\Todo' => array(
                'driver'   => 'Pdo_Sqlite',
                'database' => realpath(getcwd() . '/data/db/todo.db'),
            ),
        ),
    ),
    'zf-oauth2' => array(
        'storage' => 'ZF\\OAuth2\\Adapter\\PdoAdapter',
        'db' => array(
            'dsn_type' => 'PDO',
            'dsn' => 'sqlite:' . realpath(getcwd() . '/data/db/todo.db'),
            'username' => null,
            'password' => null,
        ),
        'storage_settings' => array(
            'user_table' => 'user',
        ),
    ),
);
