Vagrant for Zend Server 7.0EA
=============================

This directory contains a [Vagrant](http://vagrantup.com) configuration for
getting a working environment with Zend Server 7.0 Early Access, along with
some common tools for manipulating the environment if needed.

Included are:

- Linux amd_x64 image based on Ubuntu 12.04 (Precise Pangolin)
- curl, git, vim, and pip
- [HTTPie](http://httpie.org)
- [jq](http://stedolan.github.io/jq/)
- Zend Server 7.0 Early Access
  - bootstrapped, optionally with your license details
  - with DevBar enabled and configured for the forwarded ports associated with
    Zend Server

The following ports are forwarded:

- port 80 is forwarded to the host's 2222
- port 80 is forwarded to the host's 10190
- port 10081 is forwarded to the host's 10191
- port 10082 is forwarded to the host's 10192
- port 10083 is forwarded to the host's 10193

Once complete, you can access Zend Server at http://localhost:10191/ZendServer
with the credentials admin/admin.

Configuration
-------------

You can configure the image by exporting the following environment variables:

- `ZS_WEB_API_KEY_NAME`: the name of the default ZS WebAPI key; defaults to
  "vagrant".
- `ZS_WEB_API_KEY`: the actualy ZS WebAPI key (64 alnum characters); a default
  will be used if not provided.
- `ZS_ADMIN_PASSWORD`: the password to use for the ZS admin GUI; defaults to
  "admin".
- `ZS_ORDER_NUMBER`: the order number/username for your Zend Server license, if
  you wish to provide it (useful if you want to use deployment packages).
- `ZS_LICENSE_KEY`: your Zend Server license token, if you wish to provide it
  (useful if you want to use deployment packages).

> ### Note
> 
> If you wish to provide your Zend Server license, **both** `ZS_ORDER_NUMBER` and
> `ZS_LICENSE_KEY` must be provided.

Examples
--------

- Using defaults:

  ```console
  $ vagrant up
  ```

- Providing a license:

  ```console
  $ export ZS_ORDER_NUMBER=completely_foo_bar
  $ export ZS_LICENSE_KEY=THISISMADEUPANDNOTVALID
  $ vagrant up
  ```
