#!/bin/sh
ZS_WEB_API_KEY_NAME=$1
ZS_WEB_API_KEY=$2
ZS_ADMIN_PASSWORD=$3
ZS_ORDER_NUMBER=
ZS_LICENSE_KEY=
if [ $# -gt 3 ];then
    ZS_ORDER_NUMBER=$4
fi
if [ $# -gt 4 ];then
    ZS_LICENSE_KEY=$5
fi

/usr/local/zend/bin/zs-manage api-keys-add-key -n $ZS_WEB_API_KEY_NAME -s $ZS_WEB_API_KEY

if [ "$ZS_ORDER_NUMBER" -a "$ZS_LICENSE_KEY" ];then
    /usr/local/zend/bin/zs-manage bootstrap-single-server -a TRUE -p $ZS_ADMIN_PASSWORD -r FALSE -o $ZS_ORDER_NUMBER -l $ZS_LICENSE_KEY -N $ZS_WEB_API_KEY_NAME -K $ZS_WEB_API_KEY
else
    /usr/local/zend/bin/zs-manage bootstrap-single-server -a TRUE -p $ZS_ADMIN_PASSWORD -r FALSE -N $ZS_WEB_API_KEY_NAME -K $ZS_WEB_API_KEY
fi

/usr/local/zend/bin/zs-manage store-directive -d zend_statistics_extension.devbar.enable -v 1 -N $ZS_WEB_API_KEY_NAME -K $ZS_WEB_API_KEY

/usr/local/zend/bin/zs-manage store-directive -d zend_statistics_extension.devbar.zendserver_ui_url -v 'http://localhost:10191/ZendServer' -N $ZS_WEB_API_KEY_NAME -K $ZS_WEB_API_KEY

/usr/local/zend/bin/zs-manage restart-php 0 -N $ZS_WEB_API_KEY_NAME -K $ZS_WEB_API_KEY
